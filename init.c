#include <xc.h>
#include <stdio.h>
#include <stdlib.h>

#define _XTAL_FREQ 4000000.0    /*for 4mhz*/
#define I2C_SLAVE 0x27	/* was 1E Channel of i2c slave depends on soldering on back of board*/
#define TX_enable RC0

extern void Pin_Clock_Config (void);
extern void setup_comms(void);
extern void startup(void);
extern void i2c_Init(void);
extern void I2C_LCD_Command(unsigned char,unsigned char);
extern void I2C_LCD_SWrite(unsigned char,unsigned char *, char);
extern void I2C_LCD_Init(unsigned char);
extern void I2C_LCD_Pos(unsigned char,unsigned char);
extern unsigned char I2C_LCD_Busy(unsigned char);

void uart_TX(unsigned int txDat){
    TXREG = txDat;
    //__delay_ms(1);
    while(TXIF == 0){   //Is TX buffer full? (1=empty, 0=full)
        continue;
    }
}

unsigned int uart_RX(void){
    unsigned int dataReceived = 0;
    while(RCIF == 0){   //Is RX buffer full? (1=full, 0=notfull)
        continue;
    }
    RCSTA |= (1 << 5);      //Disable receiver
    dataReceived = RCREG;   //Save data received
    PIR1 |= (1 << 5);       //Clear UART receive interrupt flag
    RCSTA &= ~(1 << 5);      //Enable receiver
    
    return dataReceived;
}

void initialize(void){
    extern unsigned char  Sout[16];
    Pin_Clock_Config ();
    setup_comms();
    i2c_Init();
        //Clock and Pin Configs
	/*  ********************************I2C**************************** */
	/*  Note the I2C write to LCD uses the 8 bits of the PCF8574 chip to control the LCD  */
	/*  Connected High 4 bits are High 4 data for LCD (use in 4 bit mode)  the other 4 are     */
	/*  bit3=turn on /off bk light  bit 2= E line writes on Hi 2 Lo transition, reads Lo to Hi */
	/*  bit 2=Read write set  to 0 for write   bit 0=RS  command=0 Data=1      */
	/*  ********************************I2C**************************** */
    __delay_ms(50);
    
    I2C_LCD_Init(I2C_SLAVE); //pass I2C_SLAVE to the init function to create an instance
    I2C_LCD_Command(I2C_SLAVE, 0b00001100); //Display on, underline off, blink off
    I2C_LCD_Pos(I2C_SLAVE, 0x0); //Set Position 
    
    I2C_LCD_Command(I2C_SLAVE, 0x01); //Clear display
    sprintf(Sout, "Initializing");
    I2C_LCD_SWrite(I2C_SLAVE, Sout, 12);
    
    startup();
    
    I2C_LCD_Command(I2C_SLAVE, 0x01); //Clear display
    sprintf(Sout, "Initialized");
    I2C_LCD_SWrite(I2C_SLAVE, Sout, 11);
    
    /*Display Headers*/
    I2C_LCD_Command(I2C_SLAVE, 0x01); //Clear display
    I2C_LCD_Pos(I2C_SLAVE, 0x0); //Set Position
    sprintf(Sout, "Boom: ");
    I2C_LCD_SWrite(I2C_SLAVE, Sout, 6);
    
    I2C_LCD_Pos(I2C_SLAVE, 0x40); //Set Position
    sprintf(Sout, "Rotation: ");
    I2C_LCD_SWrite(I2C_SLAVE, Sout, 10);
    
    TX_enable = 1;    //TX enable for RS485
}
