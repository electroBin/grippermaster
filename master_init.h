#include <xc.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include  "i2c.h"
#include  "i2c_LCD.h"

#define _XTAL_FREQ 4000000.0    /*for 4mhz*/


// CONFIG1
#pragma config FOSC = INTOSC    // Oscillator Selection (INTOSC oscillator: I/O function on CLKIN pin)
#pragma config WDTE = OFF       // Watchdog Timer Enable (WDT disabled)
#pragma config PWRTE = OFF      // Power-up Timer Enable (PWRT disabled)
#pragma config MCLRE = ON       // MCLR Pin Function Select (MCLR/VPP pin function is MCLR)
#pragma config CP = OFF         // Flash Program Memory Code Protection (Program memory code protection is disabled)
#pragma config CPD = OFF        // Data Memory Code Protection (Data memory code protection is disabled)
#pragma config BOREN = ON       // Brown-out Reset Enable (Brown-out Reset enabled)
#pragma config CLKOUTEN = OFF   // Clock Out Enable (CLKOUT function is disabled. I/O or oscillator function on the CLKOUT pin)
#pragma config IESO = OFF       // Internal/External Switchover (Internal/External Switchover mode is disabled)
#pragma config FCMEN = OFF      // Fail-Safe Clock Monitor Enable (Fail-Safe Clock Monitor is disabled)

// CONFIG2
#pragma config WRT = OFF        // Flash Memory Self-Write Protection (Write protection off)
#pragma config PLLEN = OFF      // PLL Enable (4x PLL disabled)
#pragma config STVREN = ON      // Stack Overflow/Underflow Reset Enable (Stack Overflow or Underflow will cause a Reset)
#pragma config BORV = LO        // Brown-out Reset Voltage Selection (Brown-out Reset Voltage (Vbor), low trip point selected.)
#pragma config LVP = OFF        // Low-Voltage Programming Enable (High-voltage on MCLR/VPP must be used for programming)

/*Serial Configuration*/
#define BAUD 57600   //Bits per second transfer rate
#define FOSC 4000000L   //Frequency Oscillator
#define DIVIDER ((int)(FOSC/(16UL * BAUD) -1))  //Should be 25 for 9600/4MhZ
#define I2C_SLAVE 0x27	/* was 1E Channel of i2c slave depends on soldering on back of board*/
#define _XTAL_FREQ 4000000.0    /*for 4mhz*/
#define TX_enable RC0
#define neutralJawDuty 16

extern unsigned int transData;
extern unsigned int receiveData;
extern void uart_TX(unsigned int);
extern unsigned int uart_RX(void);


void Pin_Clock_Config (void){
   OSCCON  = 0x6A; /* b6..4 = 1101 = 4MHz */
   LATA     =  0x00;
   LATB     =  0x00;
   LATC     =  0x40;
   
   TRISA = 0b11011011;
           //--0-----     PORTA, 5 = green LED
           //-----0--	   PORTA, 2 = blue LED 
   TRISB = 0b11111110;
   TRISC = 0b00111110;
           //-------0    TX enable for RS485
           //------1-    PORTC, 1 = signal line coming from remote
           //-----1--    PORtC, 2 = potentiometer
           //----1---    PORTC, 3 = potentiometer
           //---1----    PORTC, 4 = UART transmit pin
           //--1-----    PORTC, 5 = UART receive pin
           //-0------    PORTC, 6 = Red LED
           //0-------    PORTC, 7 = Signal line going to remote
                
   LATA |= 1 << 5;         //Turn off green LED
   LATA |= 1 << 3;         //Turn off blue LED
   LATC |= 1 << 6;         //Turn off red LED
   
   LATA     =  0x24;  // Turn off the Green and Blue LEDs
   LATB     =  0x00;  //All 0
   LATC     =  0x40;  //Turn off Red LED
   
   ANSELA = 0b00000000;
             
   ANSELC = 0b00001100;
            //----1---     PORTC, 3 = potentiometer
            //-----1--     PORTC, 2 = potentiometer
   ANSELB = 0b00100000;
            //--1----- 	PORTB, 5 = Potentiometer input
        
   ADCON1 = 0b01000000;
            //0-------  ADFM=0 (left justified. 8MSBs are in ADRESH
                       //when conversion result is loaded
            //-100----  ADCS<0:2>, bits 4-6 =100. (1.0uS)
            //          FOSC/4=4Mhz/4 (doubles instruction cycle time)
   
   RC7 = 0;
 }

void startup(void){
    //Wait until remote pulls RC1 high
    while(RC1 == 0){
        continue;
    }
    __delay_ms(2000);
}

void setup_comms(void){
    APFCON0 = 0b10000100; //This sets pins RC5 and RC4 as RX & TX on pic16f1829
              //1-------    RXDTSEL=1, UART RX function is on RC5
              //-----1--    TXCKSEL=1, UART TX function is on RC4
    
    SPBRG = DIVIDER;
    RCSTA = 0b10010000;
            //1-------  :Serial port enabled (SPEN = 1)
            //-0------  :8-bit reception (RX9 = 0)
            //---1----  :Enable receiver (CREN = 1)
            //----0---  :Disable address detection (ADDEN = 0)
                       //all bytes are received and 9th bit can be used as
                       //parity bit 
    
    TXSTA = 0b00100100;
            //-0------  :8-bit transmission (TX9 = 0)
            //--1-----  :Enable transmission (TXEN = 1)
            //---0----  :Asynchronous mode (SYNC = 0)
            //-----0--  :Low speed baud rate (BRGH = 0)
}