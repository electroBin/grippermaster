EE-3463 (Micro 1) Final Project.
Master Controller for Gripper Arm.

Program to control the Viva board as a master controller for a remotely operated
gripper arm.  Analog joysticks signals are converted into values that are transmitted 
to the remote controller via UART/RS485.  Remote turns these values into commands to drive a servo 
motor and two stepper motors.  Master displays the directional status of the gripper arm 
on a 2x16 character LCD via i2c.

Daniel Lawton
(Group 25)

