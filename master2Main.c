/*Program to control the Viva board as a master controller for a remotely operated
 gripper arm.  Analog joysticks signals are converted into values that are transmitted 
 to the remote controller via UART/RS485.  Remote turns these values into commands to drive a servo 
 motor and two stepper motors.  Master displays the directional status of the gripper arm 
 on a 2x16 character LCD via i2c.*/
//UART_TX = RC5
//UART_RX = RC4
#include "master_init.h"

/**************************Prototypes******************************************/
//LCD
void I2C_LCD_Command(unsigned char,unsigned char);
void I2C_LCD_SWrite(unsigned char,unsigned char *, char);
void I2C_LCD_Init(unsigned char);
void I2C_LCD_Pos(unsigned char,unsigned char);
unsigned char I2C_LCD_Busy(unsigned char);
//Comm
void uart_TX(unsigned int);
unsigned int uart_RX(void);
//Startup
extern void initialize(void);
//Global variables
unsigned int receiveData;
unsigned char  Sout[16];
unsigned char * Sptr;

void main(void) {
    initialize();
    /***************************Variables**************************************/
        unsigned int ADCval;
         double jawsDuty = 16;
         double oldJawsDuty;
        unsigned int oldADCval;
        Sptr = Sout;

    while(1) {
        //Check for reset condition (remote pulls RC1 low)
        if(RC1 == 0){
            #asm
                RESET
            #endasm
        }
/**********************Gripper Jaws/AN6 Conversion*****************************/
        ADCON0 = 0b00011001;
                 //-00110--	    CHS<0:4> (bits 2-6) = 00110 = AN6 = PORTC, 2 = Potentiometer
                 //------0-     Stop AD conversion
                 //-------1	    Enable ADC
        __delay_us(10);     //Acquisition delay
        ADGO = 1;           //Start ADC
        while(ADGO){
            continue;
        }
        ADCval = (unsigned)(ADRESH); //Store 8 bit ADC result
        oldJawsDuty = jawsDuty;

        if(ADCval > 127 && jawsDuty < 18){
            jawsDuty += 0.1;
        }
        else if(ADCval < 123 && jawsDuty > 11){
            jawsDuty -= 0.1;
        }
        else{                //Account for "slop" in joystick neutral position
            jawsDuty = oldJawsDuty;
        }
        TX_enable = 1;
        uart_TX((int)jawsDuty);
/**************************End JAWS ADC****************************************/    
        
/**************************Tilt/AN7 Conversion*********************************/
        ADCON0 = 0b00011101;
                 //-00111--	    CHS<0:4> (bits 2-6) = 00111 = AN7 = PORTC, 3 = Potentiometer
                 //------0-     Stop AD conversion
                 //-------1	    Enable ADC
        __delay_us(10);     //Acquisition delay
        ADGO = 1;           //Start ADC
        while(ADGO){
            continue;
        }
        ADCval = (unsigned)(ADRESH); //Store 8 bit ADC result
        
        //Press fwd = lower 1/2 ADC range = tilt up
        //Pull back = higher 1/2 ADC range = tilt down
        if(ADCval > 130 && ADCval < 134){ //Neutral slop or
                                         //ADC input disconnected
            sprintf(Sout, "Hold ");
        }
        else if(ADCval > 133){   //tilting down
            sprintf(Sout, "Down ");
            TX_enable = 1;
            uart_TX(2);
        }
        else{   //tilting up
            sprintf(Sout, "Up   ");
            TX_enable = 1;
            uart_TX(1);
        }
        I2C_LCD_Pos(I2C_SLAVE, 0x06); //Set Position
        I2C_LCD_SWrite(I2C_SLAVE, Sout, 5);
/****************************End Tilt ADC**************************************/

/***************************Rotation/AN11 Conversion***************************/
        ADCON0 = 0b00101101;
                 //-01011--	    CHS<0:4> (bits 2-6) = 00110 = AN6 = PORTC, 2 = Potentiometer
                 //------0-     Stop AD conversion
                 //-------1	    Enable ADC
        __delay_us(10);     //Acquisition delay
        ADGO = 1;           //Start ADC
        while(ADGO){
            continue;
        }
        ADCval = (unsigned)(ADRESH); //Store 8 bit ADC result       
        
        //Press right = clockwise rotation (lower 1/2 ADC range)
        //Press left = counter-clockwise rotation (upper 1/2 ADC range)
        if(ADCval > 129 && ADCval < 133){ //Neutral slop or
                                          //ADC input disconnected
            sprintf(Sout, "Hold");
        }
        else if(ADCval > 131){   //CCW rotation
            sprintf(Sout, "CCW ");
            TX_enable = 1;
            uart_TX(5);
        }
        else{   //CW rotation
            sprintf(Sout, "CW  ");
            TX_enable = 1;
            uart_TX(4);
        }
        I2C_LCD_Pos(I2C_SLAVE, 0x4A); //Set Position
        I2C_LCD_SWrite(I2C_SLAVE, Sout, 4);
    }
}

